import React, { useEffect, useState } from "react";

function HatList(props) {
    const [hats, setHats] = useState([]);

    const handleDelete = async (hatId) => {
        const hatUrl = "http://localhost:8090/api/hats/${hatId}";
        const fetchConfig = {
            method: "delete",
            headers: {
                "Content-Type": "application/json",
            },
        };
    const hatResponse = await fetch(hatUrl, fetchConfig);
    if (hatResponse.ok) {
        console.log("Hat successfully deleted!");
        loadHats();
        }
    }

    useEffect(() => {
        loadHats()
    }, [])

    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Fabric</th>
                    <th>Style</th>
                    <th>Color</th>
                    <th>Picture URL</th>
                    <th>Location</th>
                </tr>
            </thead>
            <tbody>
                {hats.map(hat => {
                    return (
                        <tr key={hat.id}>
                            <td>{ hat.fabric }</td>
                            <td>{ hat.style_name }</td>
                            <td>{ hat.color }</td>
                            <td>{ hat.picture_url }</td>
                            <td>{ hat.location }</td>
                            <td><button type="button" onClick={() => handleDelete(hat.id)}>Delete</button></td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
    );
}
export default HatList
