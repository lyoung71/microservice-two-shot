import React, { useEffect, useState } from "react";

function HatForm() {
    const [locations, setLocations] = useState([]);

    const [formData, setFormData] = useState({
        fabric: "",
        style_name: "",
        color: "",
        picture_url: "",
        location: ""
    });

    const handleSubmit = async (event) => {
        event.preventDefault();
        const hatsUrl = "http://localhost:8090/api/hats/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                "Content-Type": "application/json",
            },
        };
        const hatsResponse = await fetch(hatsUrl, fetchConfig);
        if (hatsResponse.ok) {
            setFormData({
                fabric: "",
                style_name: "",
                color: "",
                picture_url: "",
                location: ""
            });
        }
    }

    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;

        setFormData(formData=>({
            ...formData,
            [inputName]: value
        }));
    }

    const fetchData = async () => {
        const locationsUrl = "http://localhost:8100/api/locations/";
        const locationsResponse = await fetch(locationsUrl);
        if(locationsResponse.ok) {
            const data = await locationsResponse.json();
            setLocations(data.locations);
        }
    }

    useEffect(() => {
        fetchData()
    }, []);

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new hat</h1>
                    <form onSubmit={handleSubmit} id="create-hat-form">
                        <div className="form-floating mb-3">
                            <input value={formData.fabric} onChange={handleFormChange} placeholder="Fabric" required type="text" name="fabric" id="fabric" class="form-control"></input>
                            <label htmlFor="fabric">Fabric</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={formData.style_name} onChange={handleFormChange} placeholder="Style Name" required type="text" name="style_name" id="style_name" class="form-control"></input>
                            <label htmlFor="style_name">Style Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={formData.color} onChange={handleFormChange} placeholder="Color" required type="text" name="color" id="color" class="form-control"></input>
                            <label htmlFor="color">Color</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={formData.picture_url} onChange={handleFormChange} placeholder="Picture URL" required type="text" name="picture_url" id="picture_url" class="form-control"></input>
                            <label htmlFor="picture_url">Picture URL</label>
                        </div>
                        <div className="mb-3">
                            <select onChange={handleFormChange} required name="location" id="location" class="form-select">
                                <option value={formData.location}>Choose a location</option>
                                {locations.map(location=> {
                                    return (
                                        <option key={location.id} value={location.id}>{location.id}</option>
                                    );
                                })}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default HatForm;
