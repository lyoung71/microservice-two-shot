import React, { useEffect, useState } from "react";

function ShoeList(props) {
  const [shoes, setShoes] = useState([]);

  const handleDelete = async (shoeId) => {
    const shoeUrl = `http://localhost:8080/api/shoes/${shoeId}/`;
    const fetchConfig = {
        method: "delete",
        headers: {
            "Content-Type": "application/json",
        },
    };
    const response = await fetch(shoeUrl, fetchConfig);

    if (response.ok) {
      console.log("The shoe has been deleted");
      loadShoes();
    }
  }

  async function loadShoes() {
    const response = await fetch("http://localhost:8080/api/shoes/");
    if(response.ok) {
      const data = await response.json();
      setShoes(data.shoes)
    } else {
      console.error(response);
    }
  }

  useEffect(() => {
    loadShoes()
  }, [])

  return (

      <table className="table table-striped">
          <thead>
              <tr>
                  <th>Manfacturer</th>
                  <th>Model</th>
                  <th>Color</th>
                  <th>Picture URL</th>
                  <th>Bin</th>
              </tr>
          </thead>
          <tbody>
              {shoes.map(shoe => {
              return (
                <tr key={shoe.id}>
                  <td>{ shoe.manufacturer }</td>
                  <td>{ shoe.model_name }</td>
                  <td>{ shoe.color }</td>
                  <td>{ shoe.picture_url }</td>
                  <td>{ shoe.bin }</td>
                  <td><button type="button" onClick={() => handleDelete(shoe.id)}>Delete</button></td>
                </tr>
              );
            })}
          </tbody>
        </table>
      );
  }
export default ShoeList
