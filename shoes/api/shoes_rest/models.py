from django.db import models
from django.core.exceptions import ObjectDoesNotExist
from django.urls import reverse


# Create your models here.

class BinVO(models.Model):
    href = models.CharField(max_length=100, unique=True)
    closet_name = models.CharField(max_length=100, null=True)
    bin_size = models.PositiveSmallIntegerField(null=True)
    bin_number = models.PositiveSmallIntegerField(null=True)


class Shoe(models.Model):
    manufacturer = models.CharField(max_length=150)
    model_name = models.CharField(max_length=150)
    color = models.CharField(max_length=20)
    picture_url = models.URLField(null=True, blank=True)
    bin = models.ForeignKey(
        BinVO,
        related_name="shoes",
        on_delete=models.CASCADE
    )

    def __str__(self):
        return self.model_name

    def get_url(self):
        return reverse("show_shoe_details", kwargs={"id": self.id})
