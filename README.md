# Wardrobify

Team:

* Landon - Shoes
* Daniel - Hats

## Design

## Shoes microservice

Shoe model will have the following fields:
    manufacturer, model name, color, url for picture, and bin where location exists(FK with bin)

Bin model inside wardrobe will have the following fields:
    id, href, closet_name, bin_number, and bin_size

Will create RESTful APIs to get create new shoes, return list of shoes, and delete shoes.


## Hats microservice

Allowed someone to create a hat, setting certain properties to it, such as color, fabric, and its location. Also allowed them to update a previous entry, as well as delete a previous entry.
