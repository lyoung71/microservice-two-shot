from django.urls import path
from .views import list_hats, one_hat

urlpatterns=[
    path('hats/', list_hats, name="list_hats"),
    path('hats/<int:id>/', one_hat, name="one_hat")
]
