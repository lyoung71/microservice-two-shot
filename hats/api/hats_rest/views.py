import json
from django.shortcuts import render
from .models import LocationVO, Hat
from django.http import JsonResponse
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods

class LocationVODetailEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        "href",
        "closet_name",
        "section_number",
        "shelf_number",
    ]

class HatListEncoder(ModelEncoder):
    model = Hat
    properties = [
        "fabric",
        "style_name",
        "color",
        "picture_url",
        "location",
        "id",
    ]
    encoders = {
        "location": LocationVODetailEncoder(),
    }

class HatDetailEncoder(ModelEncoder):
    model = Hat
    properties = [
        "fabric",
        "style_name",
        "color",
        "picture_url",
        "location",
        "id"
    ]
    encoders = {
        "location": LocationVODetailEncoder(),
    }

@require_http_methods(["GET","POST"])
def list_hats(request):
    if request.method == "GET":
        hats = Hat.objects.all()
        return JsonResponse(
            {'hats': hats},
            encoder=HatListEncoder
            )
    else:
        content = json.loads(request.body)
        try:
            location_id = content["location"]
            location = LocationVO.objects.get(id=location_id)
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Error: Invalid location ID"},
                status=400
            )
        hat = Hat.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False
            )

@require_http_methods(["GET", "PUT", "DELETE"])
def one_hat(request, id):
    if request.method == "GET":
        try:
            hat = Hat.objects.get(id=id)
            return JsonResponse(
                hat,
                encoder=HatDetailEncoder,
                safe=False
            )
        except Hat.DoesNotExist:
            response = JsonResponse({"message": "Error: Hat does not exist"})
            response.status_code=404
            return response
    elif request.method == "PUT":
        try:
            content = json.loads(request.body)
            hat = Hat.objects.get(id=id)
            props = [
                "fabric",
                "style_name",
                "color",
                "picture_url",
                "location"
            ]
            for prop in props:
                if prop in content:
                    setattr(hat, prop, content[prop])
            hat.save()
            return JsonResponse(
                hat,
                encoder=HatDetailEncoder,
                safe=False
            )
        except Hat.DoesNotExist:
            response = JsonResponse({"message": "Error: Hat does not exist"})
            response.status_code=404
            return response
    else:
        try:
            hat = Hat.objects.get(id=id)
            hat.delete()
            return JsonResponse(
                hat,
                encoder=HatDetailEncoder,
                safe=False
            )
        except Hat.DoesNotExist:
            response = JsonResponse({"message": "Error: Hat does not exist"})
            return response
