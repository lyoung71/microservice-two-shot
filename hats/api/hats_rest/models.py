from django.db import models

class LocationVO(models.Model):
    href = models.CharField(max_length=255, unique=True)
    closet_name = models.CharField(max_length=255)
    section_number = models.PositiveIntegerField()
    shelf_number = models.PositiveIntegerField()

class Hat(models.Model):
    fabric = models.CharField(max_length=255)
    style_name = models.CharField(max_length=255)
    color = models.CharField(max_length=255)
    picture_url = models.URLField(null=True, blank=True)
    location = models.ForeignKey(
        LocationVO,
        related_name="hats",
        on_delete=models.CASCADE,
    )
